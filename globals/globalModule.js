const fs = require('fs');


module.exports = {
    
    before: (done) => {
        console.log('before test suite')
        done();
    },

    after: (done) => {

        console.log('after test suite')
        done();
       

    },

    beforeEach: (done) => {
        console.log('before each testCase')
        done();

    },

    afterEach: (done) => {
        console.log('after each testCase')
        done();
    },

    reporter:(results, done)=>{
        fs.writeFile('testresults.json', JSON.stringify(results, null, '\t'),(err)=>{
            if(err) throw err;
            console.log('report saved')
            done();
        })
       
    }


}