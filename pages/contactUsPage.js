
module.exports = {

    elements: {
        "contactUslink":"div a[href='/contactus']",
        "typeDDL":"div select[type='native-select']",
        "typeDDLvalue":"select option[value='complaint']",
        "titlebox":"input[name='title']",
        "contentbox":"div textarea[name='content']",
        "submitbtn":"button[type='submit']",

    },
    commands: [
        {
            userCansendComplaint(title, message){
                return this
                .click('@contactUslink')
                .click('@typeDDL')
                .click('@typeDDLvalue')
                .setValue('@titlebox',title)
                .setValue('@contentbox', message)
                .click('@submitbtn')

            }

        }

    ]
}