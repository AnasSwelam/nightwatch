module.exports={
    url:"https://testing.portal.mof.fslabs.net/",

    elements:{
        "loginLink":"a.MainBar_auth-btn__3-O8e",
        "userNameBox":"input[name='username']",
        "passworkBox":"input[name='password']",
        "loginbtn":"button[type='submit']",
        "txtresult":"div[class='ProfileCard_header-content__1ZQSP'] span",
        "logoutbtn" :"button.ProfileCard_button-logout__3T14I  [class='fa fa-sign-out']",
        "logoutbtnAlert":"button[class='ProfileCard_logout-btn-model__WDj6j']",
        "logoutTxt":"a[class='MainBar_auth-btn__3-O8e']"

    },
    commands:[
        {
            userCanLogin(email,password){
                return this 
                .click('@loginLink')
                .setValue('@userNameBox', email)
                .setValue('@passworkBox', password)
                .click('@loginbtn')
            },

            userCanlogout(){
                return this
                .click('@logoutbtn')
                .click('@logoutbtnAlert')
            }

        }
    ]
}