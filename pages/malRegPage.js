module.exports={
    url:function(){
        return "https://testing.portal.mof.fslabs.net/";
    },
    elements:{
        "loginHomeLink":"a.MainBar_auth-btn__3-O8e",
        "registerLink" : "button[class='ToggleSignup_auth-tab-ctr__2qwF5 ']",
        "fullName":"input[name='fullName']",
        "userName":"input[name='username']",
        "password":"input[name='password']",
        "confPassword":"input[name='confirmPassword']",
        "email":"input[name='email']",
        "phoneNumber":"input[name='phone']",
        "JobTitleDDL":"div[class='SelectField_custom-select__1ls5g css-1o4cuih-container']",
        "JobTitleValue":"div[class=' css-11unzgr']:nth-child(1)",
        "registerbtn":"button[type='submit']",
        "txtresult":"div[class='ProfileCard_header-content__1ZQSP'] span",
        "logoutbtn" :"button.ProfileCard_button-logout__3T14I  [class='fa fa-sign-out']",


    },
    commands:[
        {
            userCanRegister(fullName,userName,password,email,phoneNumber){
                return this
                .click('@loginHomeLink')
                .click('@registerLink')
                .setValue('@fullName',fullName)
                .setValue('@userName',userName)
                .setValue('@password',password)
                .setValue('@confPassword',password)
                .setValue('@email',email)
                .setValue('@phoneNumber',phoneNumber)
                .click('@JobTitleDDL')
                .click('@JobTitleValue')
                .click('@registerbtn');
            }

        }
    ]
}